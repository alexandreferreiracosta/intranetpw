<!DOCTYPE html>
<html>
    <head>
        <title>Aniversários</title>
        <meta http-equiv="content-Type" content="text/html; charset=iso-8859-1" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../bootstrap/css/index.css" rel="stylesheet" media="screen">
        <link href="index.css" rel="stylesheet" media="screen">
        <script src="../bootstrap/js/jquery-3.1.0.min"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="../bootstrap/js/bootstrap.min.js"></script>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body style="background-image: url('../img/porti06.jpg');">

        <a href="../index.php">
            <img style="position:fixed; left:5%; top:3%; z-index:9999;" id="volta"/>
        </a>

        <div class="container" style="left:5%; position:absolute; top:1%;">
            <div class="row">
                <div class="[ col-xs-12 col-sm-offset-2 col-sm-8 ]" >
                    <ul class="event-list" id="niver">
                        <?php

                        $conexao = new PDO('mysql:dbname=intra;host=localhost', 'root', 'mypwbr4s1l');

                        $sth = $conexao->prepare('SELECT  *FROM aniversario WHERE (Day(data) >= Day(now()) AND Month(data) = Month(now())) OR (Day(data) < Day(now()) AND Month(data) > Month(now()))LIMIT 5');

                        $sth->execute();

                        while ($row = $sth->fetch()) {
                            $data = $row['data'];
                            $nome = $row['nome'];
                            $arrayData = explode("-", $data);

                            $mes_nome = "";

                            if ($arrayData[1] == 1) {
                                $mes_nome = "Jan";
                            } elseif ($arrayData[1] == 2) {
                                $mes_nome = "Fev";
                            } elseif ($arrayData[1] == 3) {
                                $mes_nome = "Mar";
                            } elseif ($arrayData[1] == 4) {
                                $mes_nome = "Abr";
                            } elseif ($arrayData[1] == 5) {
                                $mes_nome = "Mai";
                            } elseif ($arrayData[1] == 6) {
                                $mes_nome = "Jun";
                            } elseif ($arrayData[1] == 7) {
                                $mes_nome = "Jul";
                            } elseif ($arrayData[1] == 8) {
                                $mes_nome = "Ago";
                            } elseif ($arrayData[1] == 9) {
                                $mes_nome = "Set";
                            } elseif ($arrayData[1] == 10) {
                                $mes_nome = "Out";
                            } elseif ($arrayData[1] == 11) {
                                $mes_nome = "Nov";
                            } else {
                                $mes_nome = "Dez";
                            }

                            echo "<li>";
                            echo "<time datetime=''>";
                            echo "<span class='day'>$arrayData[2]</span>";
                            echo " <span class='month'>$mes_nome</span>";
                            echo " <span class='year'>$arrayData[0];</span>";
                            echo "</time>";
                            echo "<img alt='!' src='../img/banner1.jpg' />";
                            echo "<div class='info'>";
                            echo "<h2 class='title'>Aniversário de $nome!</h2>";
                            echo "<p class='desc'>Desejamos a você um Feliz Aniversário</p>";
                            echo "</div>";
                            echo "</li>";
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
        <img  id="img1">
        <img  id="img2">
        <img  id="img3">
        <img  id="img4">
    </body>
</html>
