<!DOCTYPE html>
<html>
    <head>
        <title>Cadastro de Comunicados</title>
        <meta http-equiv="content-Type" content="text/html; charset=iso-8859-1" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../bootstrap/css/index.css" rel="stylesheet" media="screen">
        <link href="../css/index.css" rel="stylesheet" media="screen">
        <script src="../bootstrap/js/jquery-3.1.0.min"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="../bootstrap/js/bootstrap.min.js"></script>
        <script src="index.js"></script>
    </head>
    <body style="background-color:#ffffff;">
    <div style="width:100%;background:#0971B2;">
    <div class="container">
    <div class="row">
      <a href="../index.php"><div class="col-sm-12" style="background:#0971B2; padding: 5px 4px 3px 4px;">
        <center><img src="../img/vv4.png"/></center>
        <center style="color:#ffffff;"><b><span>Home</span></b></center>
      </div></a>
  </div>
</div>
</div>
<hr style="height:5px; border:none; color:#000; background-color:#000000; margin-top: 0px; margin-bottom: 0px;"/>
<br>
<div class="container" style="width:100%;">
<div class="col-md-5" style="position:absolute; left:30%;">
    <div class="form-area">
        <form role="form" enctype="multipart/form-data" method="POST" action="../php/cadastro.php">
        <br style="clear:both">
                    <h1 style="margin-bottom: 15px; text-align: center;">Cadastro de Comunicados</h1>
    				<div class="form-group">
						<input type="text" class="form-control" id="nome" name="nome" placeholder="Nome" required>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="titulo" name="titulo" placeholder="Titulo" required>
					</div>
          <div class="form-group">
            <div class="fileinput fileinput-new" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span>Selecionar imagem</span><input id= "userfile" name="userfile" type="file" /></span>
					</div>
          <br>
          <div class="form-group">
          <textarea class="form-control" type="textarea" id="msg" name="msg" placeholder="Contéudo" maxlength="1200" rows="7"></textarea>
          </div>

        <button type="submit" id="submit" name="submit" class="btn btn-primary pull-right">Enviar</button>
        </form>
    </div>
    <br>
    <br>
    <br>
    <?php
    session_start();
    if(isset($_SESSION['loginErro']) == true){session_destroy(); ?>
      <div class="alert alert-danger" role="alert">
      <center><span >Erro ao cadastrar Comunicado.</span></center>
    </div>
  <?php
} ?>

<?php
if(isset($_SESSION['sucesso']) == true){session_destroy(); ?>
  <div class="alert alert-success" role="alert">
  <center><span >Comunicado Cadastro com Sucesso</span></center>
</div>
<?php
} ?>
</div>
</div>
</body>
</html>
