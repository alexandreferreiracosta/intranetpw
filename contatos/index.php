<!DOCTYPE html>
<html>
    <head>
        <title>Contatos</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../bootstrap/css/index.css" rel="stylesheet" media="screen">
        <script src="../bootstrap/js/jquery-3.1.0.min"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="../bootstrap/js/bootstrap.min.js"></script>
        <script src="index.js"></script>
        <link href="index.css" rel="stylesheet" media="screen">
        <link href="main.css?version=12" rel="stylesheet" media="screen"/>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body style="background-image: url('../img/porti06.jpg');">
        <a href="../index.php">
            <img style="position:fixed; left:5%; top:3%; z-index:9999;" id="volta"/>
        </a>

        <div class="container">
            <div class="row" id="cont" style="margin-top:2%;padding: 0 10px;">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Ramais</h3>
                            <div class="pull-right">
                                <span class="clickable filter" data-toggle="tooltip" title="Filtro de Pesquisa" data-container="body">
                                    <label style="color:#fff;">Pesquisar</label>
                                    <i class="glyphicon glyphicon-search"></i>
                                </span>
                            </div>
                        </div>
                        <div class="panel-body">
                            <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Pesquisar" />
                        </div>
                        <table class="table table-striped table-hover" id="dev-table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Departamento</th>
                                    <th>Nome</th>
                                    <th>E-mail</th>
                                    <th>Telefone</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $delimitador = ',';
                                $cerca = '"';

                                $f = fopen('ramais.csv', 'r');
                                $cont = 1;
                                if ($f) {
                                    $cabecalho = fgetcsv($f, 0, $delimitador, $cerca);
                                    echo "<tr>";
                                    while (!feof($f)) {
                                        $linha = fgetcsv($f, 0, $delimitador, $cerca);
                                        if (!$linha) {
                                            continue;
                                        }
                                        $registro = array_combine($cabecalho, $linha);

                                        $dep = $registro['department'] . PHP_EOL;
                                        $nome = $registro['firstname'] . PHP_EOL;
                                        $email = $registro['email'] . PHP_EOL;
                                        $phone = $registro['phone'] . PHP_EOL;

                                        if ($cont % 2 == 0) {
                                            echo "<td class='active'>$cont</td>";
                                            echo "<td class='active'>$dep</td>";
                                            echo "<td class='active'>$nome</td>";
                                            echo "<td class='active'>$email</td>";
                                            echo "<td class='active'>$phone</td>";
                                        } else {
                                            echo "<td class='info'>$cont</td>";
                                            echo "<td class='info'>$dep</td>";
                                            echo "<td class='info'>$nome</td>";
                                            echo "<td class='info'>$email</td>";
                                            echo "<td class='info'>$phone</td>";
                                        }

                                        $cont++;
                                        echo "</tr>";
                                    }
                                    fclose($f);
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
