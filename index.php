<!DOCTYPE html>
<html>
    <head>
        <title>Home</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/index.css" rel="stylesheet" media="screen">
        <link href="index.css" rel="stylesheet" rel="stylesheet" media="screen">
        <script src="bootstrap/js/jquery-3.1.0.min"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body style="background-image: url('img/porti06.jpg');">
        <div id="myModal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true"
             style="position:absolute; top:10%;">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" style="width: 100%;height: 100%; padding: 5%;">
                    <?php
                    $conexao = new PDO('mysql:dbname=intra;host=localhost', 'root', 'mypwbr4s1l');

                    $sth = $conexao->prepare('SELECT  *FROM comunicados ORDER BY id desc LIMIT 1');

                    $sth->execute();
                    while ($row = $sth->fetch()) {
                        $titulo = $row['titulo'];
                        $msg = $row['msg'];
                        $data = $row['data'];
                        $nova_data = implode("-", array_reverse(explode("-", trim($data))));
                        echo "<center><h2>$titulo</h2></center>";
                        echo "<center><p>$msg</p></center>";
                        echo "<center><img src='img/av1.png'/></center>";
                        echo "<b><p>Data:$nova_data</p></b>";
                    }
                    ?>
                </div>
            </div>
        </div>

        <div class="hizli-menu container-fluid" style=" margin-top: 5%;">
            <div class="hizli-menu-ic container" class="col-md-12">
                <div class="row">
                    <div class="page-header text-center">
                        <center><img class="img-responsive" src="img/img02.png"/></center>
                    </div>
                    <div class="col-md-3 mbottom">
                        <a class="btn-group" href="contatos/index.php">
                            <div class="row">
                                <div class="col-md-4 col-xs-5">
                                    <img src="img/cn.png" alt="">
                                </div>
                                <div class="col-md-8 col-xs-7">
                                    <span>Contatos</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3 mbottom">
                        <a class="btn-group" href="aniversario/index.php">
                            <div class="row">
                                <div class="col-md-4 col-xs-5">
                                    <img src="img/nv.png" alt="">
                                </div>
                                <div class="col-md-8 col-xs-7">
                                    <span>Aniversários</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3 mbottom">
                        <a class="btn-group" href="http://srv-webserv-01/glpi/index.php?noAUTO=1" target="_blank">
                            <div class="row">
                                <div class="col-md-4 col-xs-5">
                                    <img src="img/ch.png" alt="">
                                </div>
                                <div class="col-md-8 col-xs-7">
                                    <span>Chamados</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3 mbottom">
                        <a class="btn-group" href="https://webmail5.ultramail.com.br/" target="_blank">
                            <div class="row">
                                <div class="col-md-4 col-xs-5">
                                    <img src="img/email.png" alt="">
                                </div>
                                <div class="col-md-8 col-xs-7">
                                    <span>Webmail</span>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
        $(document).ready(function () {
            if (!window.sessionStorage.getItem("myModal"))
            {
                $('#myModal').modal('show');
                window.sessionStorage.setItem("myModal", "1");
            }
        });
    </script>
</html>
